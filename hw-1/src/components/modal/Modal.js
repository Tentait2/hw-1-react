import {Component} from "react";
import './Modal.scss'
import img from '../../resourses/img/cross.svg'
import modalWindowDeclarations from "../../services/modalWindowDeclarations";

class Modal extends Component{
    constructor(props) {
        super(props);

    }
    closeModal = (e) =>{
        if (e.target.className === 'background' || e.target.className ==='close-modal'){
            this.props.closeModal({showModal:false})
        }
    }
    render() {
        const {id, header, modalText, backgroundColor, closeButton, buttons} = this.props.data
        return (
            <div className={'background'} onClick={this.closeModal}>
                <div key={id} className={'modal'} style={{backgroundColor}}>
                    <h1>{header}</h1>
                    <p>{modalText}</p>
                    {closeButton ? <img className={'close-modal'} src={img} alt="#"/> : null}
                    {buttons}
                </div>
            </div>
        )
    }
}

export default Modal