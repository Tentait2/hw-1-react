import {Component} from "react";
import './Button.scss';

class Button extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data, backgroundColor, text, openModal } = this.props;
        return <button data-modal={data} className={'button-primary'} style={{backgroundColor}} onClick={openModal}>{text}</button>;
    }
}

export default Button