import './App.scss';
import Button from "./components/button/Button";
import {Component} from "react";
import Modal from "./components/modal/Modal";
import modalWindowDeclarations from "./services/modalWindowDeclarations";

class App extends Component {
    state = {
        showModal : false,
        activeModal : {}
    }


    openModal = (e) =>{
        const modalID = e.target.getAttribute('data-modal')
        const modalDeclaration = modalWindowDeclarations.find(item => item.modalId === modalID);
        this.setState({
            showModal: true,
            activeModal: modalDeclaration,
        })
    }

    closeModalInfo = (state) =>{
        this.setState(state)
    }

    render() {
        return (
            <>
                <Button data = {'modalID1'} backgroundColor={'salmon'} text={`open first modal`} openModal={this.openModal}/>
                <Button data = {'modalID2'} backgroundColor={'#98ffe8'} text={`open second modal`} openModal={this.openModal}/>
                {this.state.showModal ? <Modal data ={this.state.activeModal} closeModal = {this.closeModalInfo}/> : null}
            </>
        )
    }
}

export default App;
