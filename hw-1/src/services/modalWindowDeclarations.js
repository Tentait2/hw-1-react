 const modalWindowDeclarations = [
    {
        modalId: 'modalID1',
        header: 'Do you want to delete this file?',
        modalText: 'Are you sure you want to delete it?',
        backgroundColor: 'salmon',
        closeButton : true,
        buttons : [<button>Ok</button>, <button>Cancel</button>]
    },
    {
        modalId: 'modalID2',
        header: 'You are in the good mood?',
        modalText: 'I hope you are in the good mood!',
        backgroundColor: '#98ffe8',
        closeButton : false,
        buttons : [<button>Yes, i'm okey</button>, <button>No, i have some problems</button>]
    }
]
 export default modalWindowDeclarations